---
date: 2024-07-09T12:04:00+08:00
authors: 
  - "lunny"
title: "Gitea 1.22.1 is released"
tags: ["release"]
draft: false
coverImageRelease: 1.22.1
---

We are proud to present the release of Gitea version 1.22.1.

We highly encourage users to update to this version for some important bug-fixes.

We have merged [80](https://github.com/go-gitea/gitea/pulls?q=is%3Apr+milestone%3A1.22.1+is%3Amerged) pull requests to release this version.

<!-- Security Thanks! -->

We would like to give a special thanks to [Catalin Iovita](https://github.com/catalin-iovita) and [Alexandru Postolache](https://github.com/alex-postolache) for reporting the security issue [CVE-2024-6886](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6886) which is fixed by [https://github.com/go-gitea/gitea/pull/31200](https://github.com/go-gitea/gitea/pull/31200). Thanks to [@wxiaoguang](https://github.com/wxiaoguang) for fixing the problem.

Thanks to the many people who have reported the "regreSSHion" CVE with OpenSSH sshd server to us. When it was announced we investigated and found that since we are using Alpine-based Docker images which use the musl implementation of libc we are not affected. The OpenSSH package will be updated to ensure that vulnerability scanners do not present false positives.

You can download one of our pre-built binaries from our [downloads page](https://dl.gitea.com/gitea/1.22.1/) - make sure to select the correct platform! For further details on how to install, follow our [installation guide](https://docs.gitea.com/installation/install-from-binary).

We would also like to thank all of our supporters on [Open Collective](https://opencollective.com/gitea) who are helping to sustain us financially.

**Are you looking for a seamless, hassle-free solution to manage your Git repositories? Look no further! [Gitea Cloud](https://cloud.gitea.com) is here to revolutionize your development experience.**

<!--more-->

## Changelog

## [1.22.1](https://github.com/go-gitea/gitea/releases/tag/v1.22.1) - 2024-07-04

<!-- Changelog Details -->

* SECURITY
  * Add replacement module for `mholt/archiver` ([#31267](https://github.com/go-gitea/gitea/pull/31267)) ([#31270](https://github.com/go-gitea/gitea/pull/31270))
* API
  * Fix missing images in editor preview due to wrong links ([#31299](https://github.com/go-gitea/gitea/pull/31299)) ([#31393](https://github.com/go-gitea/gitea/pull/31393))
  * Fix duplicate sub-path for avatars ([#31365](https://github.com/go-gitea/gitea/pull/31365)) ([#31368](https://github.com/go-gitea/gitea/pull/31368))
  * Reduce memory usage for chunked artifact uploads to MinIO ([#31325](https://github.com/go-gitea/gitea/pull/31325)) ([#31338](https://github.com/go-gitea/gitea/pull/31338))
  * Remove sub-path from container registry realm ([#31293](https://github.com/go-gitea/gitea/pull/31293)) ([#31300](https://github.com/go-gitea/gitea/pull/31300))
  * Fix NuGet Package API for $filter with Id equality ([#31188](https://github.com/go-gitea/gitea/pull/31188)) ([#31242](https://github.com/go-gitea/gitea/pull/31242))
  * Add an immutable tarball link to archive download headers for Nix ([#31139](https://github.com/go-gitea/gitea/pull/31139)) ([#31145](https://github.com/go-gitea/gitea/pull/31145))
  * Add missed return after `ctx.ServerError` ([#31130](https://github.com/go-gitea/gitea/pull/31130)) ([#31133](https://github.com/go-gitea/gitea/pull/31133))
* BUGFIXES
  * Fix avatar radius problem on the new issue page ([#31506](https://github.com/go-gitea/gitea/pull/31506)) ([#31508](https://github.com/go-gitea/gitea/pull/31508))
  * Fix overflow menu flickering on mobile ([#31484](https://github.com/go-gitea/gitea/pull/31484)) ([#31488](https://github.com/go-gitea/gitea/pull/31488))
  * Fix poor table column width due to breaking words ([#31473](https://github.com/go-gitea/gitea/pull/31473)) ([#31477](https://github.com/go-gitea/gitea/pull/31477))
  * Support relative paths to videos from Wiki pages ([#31061](https://github.com/go-gitea/gitea/pull/31061)) ([#31453](https://github.com/go-gitea/gitea/pull/31453))
  * Fix new issue/pr avatar ([#31419](https://github.com/go-gitea/gitea/pull/31419)) ([#31424](https://github.com/go-gitea/gitea/pull/31424))
  * Increase max length of org team names from 30 to 255 characters ([#31410](https://github.com/go-gitea/gitea/pull/31410)) ([#31421](https://github.com/go-gitea/gitea/pull/31421))
  * Fix line number width in code preview ([#31307](https://github.com/go-gitea/gitea/pull/31307)) ([#31316](https://github.com/go-gitea/gitea/pull/31316))
  * Optimize runner-tags layout to enhance visual experience ([#31258](https://github.com/go-gitea/gitea/pull/31258)) ([#31263](https://github.com/go-gitea/gitea/pull/31263))
  * Fix overflow on push notification ([#31179](https://github.com/go-gitea/gitea/pull/31179)) ([#31238](https://github.com/go-gitea/gitea/pull/31238))
  * Fix overflow on notifications ([#31178](https://github.com/go-gitea/gitea/pull/31178)) ([#31237](https://github.com/go-gitea/gitea/pull/31237))
  * Fix overflow in issue card ([#31203](https://github.com/go-gitea/gitea/pull/31203)) ([#31225](https://github.com/go-gitea/gitea/pull/31225))
  * Split sanitizer functions and fine-tune some tests ([#31192](https://github.com/go-gitea/gitea/pull/31192)) ([#31200](https://github.com/go-gitea/gitea/pull/31200))
  * use correct l10n string ([#31487](https://github.com/go-gitea/gitea/pull/31487)) ([#31490](https://github.com/go-gitea/gitea/pull/31490))
  * Fix dropzone JS error when attachment is disabled ([#31486](https://github.com/go-gitea/gitea/pull/31486))
  * Fix web notification icon not updated once you read all notifications ([#31447](https://github.com/go-gitea/gitea/pull/31447)) ([#31466](https://github.com/go-gitea/gitea/pull/31466))
  * Switch to "Write" tab when edit comment again ([#31445](https://github.com/go-gitea/gitea/pull/31445)) ([#31461](https://github.com/go-gitea/gitea/pull/31461))
  * Fix the link for .git-blame-ignore-revs bypass ([#31432](https://github.com/go-gitea/gitea/pull/31432)) ([#31442](https://github.com/go-gitea/gitea/pull/31442))
  * Fix the wrong line number in the diff view page when expanded twice. ([#31431](https://github.com/go-gitea/gitea/pull/31431)) ([#31440](https://github.com/go-gitea/gitea/pull/31440))
  * Fix labels and projects menu overflow on issue page ([#31435](https://github.com/go-gitea/gitea/pull/31435)) ([#31439](https://github.com/go-gitea/gitea/pull/31439))
  * Fix Account Linking UpdateMigrationsByType  ([#31428](https://github.com/go-gitea/gitea/pull/31428)) ([#31434](https://github.com/go-gitea/gitea/pull/31434))
  * Fix markdown math brackets render problem ([#31420](https://github.com/go-gitea/gitea/pull/31420)) ([#31430](https://github.com/go-gitea/gitea/pull/31430))
  * Fix rendered wiki page link ([#31398](https://github.com/go-gitea/gitea/pull/31398)) ([#31407](https://github.com/go-gitea/gitea/pull/31407))
  * Fix natural sort ([#31384](https://github.com/go-gitea/gitea/pull/31384)) ([#31394](https://github.com/go-gitea/gitea/pull/31394))
  * Allow downloading attachments of draft releases ([#31369](https://github.com/go-gitea/gitea/pull/31369)) ([#31380](https://github.com/go-gitea/gitea/pull/31380))
  * Fix repo graph JS ([#31377](https://github.com/go-gitea/gitea/pull/31377))
  * Fix incorrect localization `explorer.go` ([#31348](https://github.com/go-gitea/gitea/pull/31348)) ([#31350](https://github.com/go-gitea/gitea/pull/31350))
  * Fix hash render end with colon ([#31319](https://github.com/go-gitea/gitea/pull/31319)) ([#31346](https://github.com/go-gitea/gitea/pull/31346))
  * Fix line number widths ([#31341](https://github.com/go-gitea/gitea/pull/31341)) ([#31343](https://github.com/go-gitea/gitea/pull/31343))
  * Fix navbar `+` menu flashing on page load ([#31281](https://github.com/go-gitea/gitea/pull/31281)) ([#31342](https://github.com/go-gitea/gitea/pull/31342))
  * Fix adopt repository has empty object name in database ([#31333](https://github.com/go-gitea/gitea/pull/31333)) ([#31335](https://github.com/go-gitea/gitea/pull/31335))
  * Delete legacy cookie before setting new cookie ([#31306](https://github.com/go-gitea/gitea/pull/31306)) ([#31317](https://github.com/go-gitea/gitea/pull/31317))
  * Fix some URLs whose sub-path is missing ([#31289](https://github.com/go-gitea/gitea/pull/31289)) ([#31292](https://github.com/go-gitea/gitea/pull/31292))
  * Fix admin oauth2 custom URL settings ([#31246](https://github.com/go-gitea/gitea/pull/31246)) ([#31247](https://github.com/go-gitea/gitea/pull/31247))
  * Make pasted "img" tag has the same behavior as markdown image ([#31235](https://github.com/go-gitea/gitea/pull/31235)) ([#31243](https://github.com/go-gitea/gitea/pull/31243))
  * Fix agit checkout command line hint & fix ShowMergeInstructions checking ([#31219](https://github.com/go-gitea/gitea/pull/31219)) ([#31222](https://github.com/go-gitea/gitea/pull/31222))
  * Fix the possible migration failure on 286 with postgres 16 ([#31209](https://github.com/go-gitea/gitea/pull/31209)) ([#31218](https://github.com/go-gitea/gitea/pull/31218))
  * Fix branch order ([#31174](https://github.com/go-gitea/gitea/pull/31174)) ([#31193](https://github.com/go-gitea/gitea/pull/31193))
  * Fix markup preview ([#31158](https://github.com/go-gitea/gitea/pull/31158)) ([#31166](https://github.com/go-gitea/gitea/pull/31166))
  * Fix push multiple branches error with tests ([#31151](https://github.com/go-gitea/gitea/pull/31151)) ([#31153](https://github.com/go-gitea/gitea/pull/31153))
  * Fix API repository object format missed ([#31118](https://github.com/go-gitea/gitea/pull/31118)) ([#31132](https://github.com/go-gitea/gitea/pull/31132))
  * Fix missing memcache import ([#31105](https://github.com/go-gitea/gitea/pull/31105)) ([#31109](https://github.com/go-gitea/gitea/pull/31109))
  * Upgrade `github.com/hashicorp/go-retryablehttp` ([#31499](https://github.com/go-gitea/gitea/pull/31499))
  * Fix double border in system status table ([#31363](https://github.com/go-gitea/gitea/pull/31363)) ([#31401](https://github.com/go-gitea/gitea/pull/31401))
  * Fix bug filtering issues which have no project ([#31337](https://github.com/go-gitea/gitea/pull/31337)) ([#31367](https://github.com/go-gitea/gitea/pull/31367))
  * Fix #31185 try fix lfs download from bitbucket failed ([#31201](https://github.com/go-gitea/gitea/pull/31201)) ([#31329](https://github.com/go-gitea/gitea/pull/31329))
  * Add nix flake for dev shell ([#30967](https://github.com/go-gitea/gitea/pull/30967)) ([#31310](https://github.com/go-gitea/gitea/pull/31310))
  * Fix and clean up `ConfirmModal` ([#31283](https://github.com/go-gitea/gitea/pull/31283)) ([#31291](https://github.com/go-gitea/gitea/pull/31291))
  * Optimize repo-list layout to enhance visual experience ([#31272](https://github.com/go-gitea/gitea/pull/31272)) ([#31276](https://github.com/go-gitea/gitea/pull/31276))
  * fixed the dropdown menu for the top New button to expand to the left ([#31273](https://github.com/go-gitea/gitea/pull/31273)) ([#31275](https://github.com/go-gitea/gitea/pull/31275))
  * Fix Activity Page Contributors dropdown ([#31264](https://github.com/go-gitea/gitea/pull/31264)) ([#31269](https://github.com/go-gitea/gitea/pull/31269))
  * fix: allow actions artifacts storage migration to complete succesfully ([#31251](https://github.com/go-gitea/gitea/pull/31251)) ([#31257](https://github.com/go-gitea/gitea/pull/31257))
  * Make blockquote attention recognize more syntaxes ([#31240](https://github.com/go-gitea/gitea/pull/31240)) ([#31250](https://github.com/go-gitea/gitea/pull/31250))
  * Remove .segment from .project-column ([#31204](https://github.com/go-gitea/gitea/pull/31204)) ([#31239](https://github.com/go-gitea/gitea/pull/31239))
  * Ignore FindRecentlyPushedNewBranches err ([#31164](https://github.com/go-gitea/gitea/pull/31164)) ([#31171](https://github.com/go-gitea/gitea/pull/31171))
  * Use vertical layout for multiple code expander buttons ([#31122](https://github.com/go-gitea/gitea/pull/31122)) ([#31152](https://github.com/go-gitea/gitea/pull/31152))
  * Remove duplicate `ProxyPreserveHost` in Apache httpd doc ([#31143](https://github.com/go-gitea/gitea/pull/31143)) ([#31147](https://github.com/go-gitea/gitea/pull/31147))
  * Improve mobile review ui ([#31091](https://github.com/go-gitea/gitea/pull/31091)) ([#31136](https://github.com/go-gitea/gitea/pull/31136))
  * Fix DashboardRepoList margin ([#31121](https://github.com/go-gitea/gitea/pull/31121)) ([#31128](https://github.com/go-gitea/gitea/pull/31128))
  * Update pip related commands for docker ([#31106](https://github.com/go-gitea/gitea/pull/31106)) ([#31111](https://github.com/go-gitea/gitea/pull/31111))

## Contributors

* [@6543](https://github.com/6543)
* [@bohde](https://github.com/bohde)
* [@brechtvl](https://github.com/brechtvl)
* [@charles7668](https://github.com/charles7668)
* [@CyberFlameGO](https://github.com/CyberFlameGO)
* [@HorlogeSkynet](https://github.com/HorlogeSkynet)
* [@kerwin612](https://github.com/kerwin612)
* [@kiatt210](https://github.com/kiatt210)
* [@Mic92](https://github.com/Mic92)
* [@LazyDodo](https://github.com/LazyDodo)
* [@lunny](https://github.com/lunny)
* [@rayden84](https://github.com/rayden84)
* [@sergeyvfx](https://github.com/sergeyvfx)
* [@silverwind](https://github.com/silverwind)
* [@Sumit189](https://github.com/Sumit189)
* [@tdesveaux](https://github.com/tdesveaux)
* [@techknowlogick](https://github.com/techknowlogick)
* [@TheBrokenRail](https://github.com/TheBrokenRail)
* [@tobiasbp](https://github.com/tobiasbp)
* [@wxiaoguang](https://github.com/wxiaoguang)
* [@yp05327](https://github.com/yp05327)
* [@Zettat123](https://github.com/Zettat123)
* [@Zoupers](https://github.com/Zoupers)
